import React, { Component } from 'react';
import Journee from './Journee';

import api from '../../../../../helpers/api';

class JourneeContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataJourneeBebe: [],
        };
    }

    async componentDidMount(){
        window.scrollTo(0, 0);
        const dataJourneeBebe = await api.getServicePetiteEnfance("journee_bebe");
        
        this.setState({
            dataJourneeBebe: dataJourneeBebe.services,
        })

        
        console.log("state:",this.state);
    }
    render() {
        
        return (
            <Journee
                dataJourneeBebe={this.state.dataJourneeBebe}
            />
        );
    }
}

export default JourneeContainer;
