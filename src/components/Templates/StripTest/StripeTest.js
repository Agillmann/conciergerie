import React, { Component } from 'react';
import Checkout from './Checkout';

class StripeTest extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Payment Test</h2>
        </div>
        <p className="App-intro">
          <Checkout
            name={'The Road to learn React'}
            description={'Only the Book'}
            amount={50}
          />
        </p>
      </div>
    );
  }
}

export default StripeTest;