import React from 'react';

import styled from '@emotion/styled';
import colors from '../../../helpers/colors';

//////////////////// STYLES 

const Check = styled.i`
    margin-right: 10px;
    color: ${colors.white};
`

const AbonnementContent = styled.div`
    padding: 0px 20px;
`


const OffreContent = styled.div`
    margin: 20px 0;
`

////////// TYPO 

// const TextMembre = styled.h3`
//     color: ${colors.white};
//     font-family: 'Avenir';
//     font-weight: 900;
//     font-size: 1.5rem;
//     margin: 0;
// `

const TextMembreBis = styled.h3`
    color: ${colors.white};
    font-family: 'Avenir';
    font-weight: 900;
    font-size: 2em;
    margin: 5px 0 15px 0;
`

const TextPrice = styled.p`
    color: ${colors.white};
    font-family: 'Avenir';
    font-weight: 900;
    font-size: 1em;
    margin: 0;
`

const TextPriceBis = styled.p`
    color: ${colors.primary_bis};
    font-family: 'Avenir';
    font-weight: 900;
    font-size: 1em;
    margin: 5px 0 10px 0;
`

const TextOffre = styled.p`
    color: ${colors.white};
    font-family: 'Avenir';
    font-weight: 900;
    font-size: 1em;
    margin: 0;
`

//////////////////// VIEW 

const Abonnement = ({data, color, colorbis}) => (
    <div className="row">
        <AbonnementContent className="col-xs-12">
            <TextMembreBis color={color}>Platinium</TextMembreBis>
            <TextPrice color={color}>120€ / mois / famille</TextPrice>
            <TextPriceBis colorbis={colorbis}>Résiliable sans préavis</TextPriceBis>
            <TextPriceBis>Ou</TextPriceBis>
            <TextPrice color={color}>1200€ / an / famille</TextPrice>
            <TextPriceBis colorbis={colorbis}>soit 2 mois offert !</TextPriceBis>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Concierge privé personnel 7j/7</TextOffre>
            </OffreContent>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Joignable par téléphone, sms et WhatApp</TextOffre>
            </OffreContent>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Accès aux services et prestations 7j/7</TextOffre>
            </OffreContent>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Tarif préférentiel pour toutes prestations</TextOffre>
            </OffreContent>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Prioritaire pour les stages de vancances scolaires</TextOffre>
            </OffreContent>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Demande et offre sur mesure</TextOffre>
            </OffreContent>
            <OffreContent className="row top-xs">
                <Check className="fas fa-check col-xs-1"></Check>
                <TextOffre className="col-xs-10 start-xs top-xs">Demande illimitée</TextOffre>
            </OffreContent>
        </AbonnementContent>
    </div>
)

export default Abonnement;